Rails.application.routes.draw do
  #get 'public_meets/new'

  # get 'public_posts/index'
  root 'public_posts#index'

  #get 'public_posts/show'

  #get 'public_posts/posts_category'

  get 'citas', to: 'public_meets#new', as: 'citas'
  get 'categorias/:id', to: 'public_posts#posts_category', as: 'posts_category'
  get 'articluos/:url', to: 'public_posts#show', as: 'posts_show'

  post 'create_meet', to: 'public_meets#create_meet', as: 'create_meet'
  post 'create_comment', to: 'public_posts#create_comment', as: 'create_comment'

  devise_for :users
  resources :meets
  resources :ads
  resources :comments
  resources :categories
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
