class PublicMeetsController < ApplicationController

  layout 'public'

  def new
    @meet = Meet.new
  end

  def create_meet
    @meet = Meet.new name: params[:name], meeting: params[:meeting], subject: params[:subject]
    if @meet.save
      redirect_back(fallback_location: root_path, notice: 'La cita fue solicitada y enviada al admin')
    end
  end
end
