class PublicPostsController < ApplicationController
  layout 'public'
  def index
    @posts = Post.publisheds
  end

  def show
    @post = Post.find_by url: params[:url]
    @comments = @post.comments
    @comment = Comment.new
  end

  def posts_category
    @category = Category.find(params[:id])
    @posts = @category.posts.publisheds
  end

  def create_comment
    @comment = Comment.new name: params[:name], content: params[:content], post_id: params[:post_id]
    if @comment.save
      redirect_back(fallback_location: root_path, notice: 'El comnetario fue creado exitosamente')
    end
  end
end
