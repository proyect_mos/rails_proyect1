class Post < ApplicationRecord
  has_many :comments
  belongs_to :category
  belongs_to :user

  scope :publisheds, -> {where status: true}
  scope :orden, -> {order name}


end